# DeepL Autocorrect

This program uses deepl.com-ai to translate the text into English (and France) and back.

The original text ist saved into `original.txt` and the translated into `changed.{en,fr}.txt`.

## Dependencies:
- [pydeepl](https://github.com/EmilioK97/pydeepl)
	- `pip install pydeepl`

## Run:
Run it like in the picture:

![screenshot](screenshot_01.png)
