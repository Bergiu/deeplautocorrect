import pydeepl
import sys
import fileinput

original = ""
sp = []

exi = False
if len(sys.argv) <= 1:
    exi = True
    for line in fileinput.input():
        exi = False
        original += line
    sp.append(original)
else:
    for arg in sys.argv[1:]:
        sp.append(arg)


if exi:
    print("What should I translate?")
    sys.exit(0)

from_language = 'DE'
to_language_fr = 'FR'
to_language_en = 'EN'

changed_en = []
changed_fr = []

try:
    translation = pydeepl.translate("".join(sp), to_language_en, from_lang=from_language)
    changed_part = pydeepl.translate(translation, from_language, to_language_en)
    changed_en.append(changed_part)
except pydeepl.pydeepl.TranslationError as e:
    print("ERROR")
    print(e)
    pass

try:
    translation = pydeepl.translate("".join(sp), to_language_fr, from_lang=from_language)
    changed_part = pydeepl.translate(translation, from_language, to_language_fr)
    changed_fr.append(changed_part)
except pydeepl.pydeepl.TranslationError as e:
    print("ERROR")
    print(e)
    pass

with open("original.txt", "w") as f:
    f.write("\n".join(sp))

if len("".join(changed_en)) > 0:
    with open("changed.en.txt", "w") as f:
        f.write("\n".join(changed_en))
if len("".join(changed_fr)) > 0:
    with open("changed.fr.txt", "w") as f:
        f.write("\n".join(changed_fr))
