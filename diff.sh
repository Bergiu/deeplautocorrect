#!/bin/bash
echo "$@" | python main.py
echo "------ EN ------"
wdiff -n original.txt changed.en.txt | colordiff
echo -e "\n------ FR ------"
wdiff -n original.txt changed.fr.txt | colordiff
